/*
 * Copyright (c) 2013, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/*
 *  ======== Sem_posix.c ========
 */

/* This define must precede inclusion of any xdc header files */
#define Registry_CURDESC ti_sdo_ce_osal_Sem_desc

#include <xdc/std.h>
#include <xdc/runtime/Assert.h>
#include <xdc/runtime/Diags.h>
#include <xdc/runtime/Log.h>
#include <xdc/runtime/Registry.h>

#include <semaphore.h>

#include <ti/sdo/ce/osal/Memory.h>
#include <ti/sdo/ce/osal/Global.h>

#include <ti/sdo/ce/osal/Sem.h>

#define _Sem_SEMCOUNT 0 /* Array index of semaphore holding the count */
#define _Sem_REFCOUNT 1 /* Array index of semaphore holding the ref count */

#define NUMSEMS 2 /* Size of semaphore array */



/*
 *  ======== Sem_Obj ========
 */
typedef struct Sem_Obj {
    sem_t   sem;
    Int     id;     /* ID of IPC semaphore */
} Sem_Obj;

/*
 *  REMINDER: If you add an initialized static variable, reinitialize it at
 *  cleanup
 */

Registry_Desc ti_sdo_ce_osal_Sem_desc;
static Int regInit = 0;     /* Registry_addModule() called */
static Int curInit = 0;

static Void    cleanup(Void);

/*
 *  ======== Sem_create ========
 */
Sem_Handle Sem_create(Int key, Int count)
{
    Int     pshared = 0;  /* Semaphore is not shared by different processes */
    Sem_Obj *sem;

    Log_print1(Diags_ENTRY, "[+E] Sem_create> count: %d", (IArg)count);

    if ((sem = (Sem_Obj *)Memory_alloc(sizeof(Sem_Obj), NULL)) == NULL) {
        Log_print0(Diags_USER7, "[+7] Sem_create> Memory_alloc failed");
        return (NULL);
    }

    if (sem_init(&(sem->sem), pshared, count) != 0) {
        Log_print0(Diags_USER7, "[+7] Sem_create> sem_init() failed");
        Memory_free(sem, sizeof(Sem_Obj), NULL);
        return (NULL);
    }

    Log_print1(Diags_EXIT, "[+X] Leaving Sem_create> sem[0x%x]", (IArg)sem);

    return (sem);
}

/*
 *  ======== Sem_delete ========
 */
Void Sem_delete(Sem_Handle sem)
{
    Log_print1(Diags_ENTRY, "[+E] Entered Sem_delete> sem[0x%x]", (IArg)sem);

    if (sem != NULL) {
        sem_destroy(&(sem->sem));
        Memory_free(sem, sizeof(Sem_Obj), NULL);
    }

    Log_print0(Diags_EXIT, "[+X] Leaving Sem_delete>");
}

/*
 *  ======== Sem_getCount ========
 */
Int Sem_getCount(Sem_Handle sem)
{
    Int      count = -1;

    if (sem_getvalue(&(sem->sem), &count) != 0) {
        Log_print0(Diags_USER7, "[+7] Sem_getCount> sem_getvalue() failed");
    }

    return (count);
}

/*
 *  ======== Sem_init ========
 */
Void Sem_init(Void)
{
    Registry_Result   result;

    /*
     *  No need to reference count for Registry_addModule(), since there
     *  is no way to remove the module.
     */
    if (regInit == 0) {
        /* Register this module for logging */
        result = Registry_addModule(&ti_sdo_ce_osal_Sem_desc, Sem_MODNAME);
        Assert_isTrue(result == Registry_SUCCESS, (Assert_Id)NULL);

        if (result == Registry_SUCCESS) {
            /* Set the diags mask to the CE default */
            CESettings_init();
            CESettings_setDiags(Sem_MODNAME);
        }
        regInit = 1;
    }

    if (curInit++ == 0) {
        Global_atexit((Fxn)cleanup);
    }
}

/*
 *  ======== Sem_pend ========
 */
Int Sem_pend(Sem_Handle sem, UInt32 timeout)
{
    Int    status = Sem_EOK;

    Log_print2(Diags_ENTRY, "[+E] Entered Sem_pend> sem[0x%x] timeout[0x%x]",
            (IArg)sem, (IArg)timeout);

    /* Timeouts not supported yet */
    Assert_isTrue(timeout == Sem_FOREVER, (Assert_Id)NULL);

    sem_wait(&(sem->sem)); /* TODO: can try sem_timedwait */

    Log_print2(Diags_EXIT, "[+X] Leaving Sem_pend> sem[0x%x] status[%d]",
            (IArg)sem, (IArg)status);

    return (status);
}

/*
 *  ======== Sem_post ========
 */
Void Sem_post(Sem_Handle sem)
{
    Log_print1(Diags_ENTRY, "[+E] Entered Sem_post> sem[0x%x]", (IArg)sem);

    sem_post(&(sem->sem));

    Log_print1(Diags_EXIT, "[+X] Leaving Sem_post> sem[0x%x]", (IArg)sem);
}

/*
 *  ======== cleanup ========
 */
static Void cleanup(Void)
{
    Assert_isTrue(curInit > 0, (Assert_Id)NULL);

    if (--curInit == 0) {
        /* Nothing to do */
    }
}
